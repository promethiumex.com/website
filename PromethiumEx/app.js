var express        = require("express"),
    app            = express(),
    bodyParser     = require("body-parser"),
    cookieParser   = require("cookie-parser"),
    methodOverride = require("method-override");
    
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname));
app.use(methodOverride('_method'));
app.use(cookieParser('secret'));

// Restful routing

app.get("/", function(req, res){
    res.render("index");
});

// server Listen 

var port = process.env.PORT || 3000;

app.listen(port, process.env.IP, function(){
   console.log("The server has started!");
});